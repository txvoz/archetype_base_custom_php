function viewResource(obj) {
    var url = $(obj).data("url");
    window.location.replace(url);
}

function deleteResource(obj) {
    if ($(obj).data("ep") && $(obj).data("rs")) {
        var resource = $(obj).data("ep") + "&acc=" + $(obj).data("rs");
        console.log("Delete==>", resource);
        if (confirm("¿Desea eliminar el recurso?")) {
            httpRestFulClient(resource, null, "GET", function (r) {
                createMessage(r);
                loadDataList();
            });
        }
    } else {
        alert("No se ha definido el recurso a eliminar!");
    }
}

function loadDataForm() {
    $('*[data-eprole="form"]').each(function () {
        var acc = getParameterByName("acc");
        if (acc === null) {
            acc = $(this).data("acc");
        }
        var form = this;
        var endpoint = $(this).attr("action");
        //var resource = endpoint + "/" + acc;
        var resource = endpoint + "&acc=" + acc;
        httpRestFul(resource, {}, "GET", function (r) {
            setDataValue(form, r.data);
            var strFnDs = $(form).data("source");
            if (strFnDs) {
                var fn = eval(strFnDs);
                fn(r.data);
            }
        });
    });
}

function loadDataTable(deprole, r = {}, endpoint = "") {
    //Funcion de viewresource
    var fnView = "viewResource";
    var strFnView = $(deprole).data("fnview");
    if (strFnView) {
        fnView = strFnView;
    }
    //Se recupera el callback de la lista  
    var strCb = $(deprole).data("cb");
    var cb = null;
    if (strCb) {
        try {
            cb = eval(strCb);
        } catch (e) {
            console.log(e);
        }
    }
    //Funcion de load
    var strLoad = $(deprole).data("load");
    var load_ = null;
    if (strLoad) {
        try {
            load_ = eval(strLoad);
        } catch (e) {
            console.log(e);
        }
    }
    //***********************
    var target_ = $(deprole).find(".list");
    //***********************
    var arrayEp = endpoint.split("/");
    //***********************
    if (arrayEp.length > 0) {
        endpoint = arrayEp[0];
    }
    if (cb) {
        cb(r.data);
    }
    if (load_) {
        load_(r, deprole);
    }
    var props = $(deprole).find("*[data-property]");
    var dataBtns = $(deprole).find("*[data-btns]");
    var bdelete = false;
    var bedit = false;
    var bdetail = false;
    var ckdetail = "";
    if (dataBtns.size() > 0) {
        dataBtns = $(dataBtns)[0];
        var strBt = $(dataBtns).data("btns");
        if (strBt.search("delete") > -1) {
            bdelete = true;
        }
        if (strBt.search("edit") > -1) {
            bedit = true;
        }
        if (strBt.search("detail") > -1) {
            ckdetail = $(dataBtns).data("detail");
            bdetail = true;
        }
    }

    var strPrint = $(deprole).data("print");
    var cbPrint = null;
    if (strPrint) {
        try {
            cbPrint = eval(strPrint);
        } catch (e) {
            cbPrint = null;
            console.log(e);
        }
    }

    if (cbPrint !== null) {
        cbPrint(r);
    } else {
        var html = "";
        $(r.data).each(function () {
            var obj = this;
            html += "<tr>";
            $(props).each(function () {
                //Funcion para pintar otra funcionalidad
                var getter = $(this).data("get");
                if (getter) {
                    var fnget = eval(getter);
                    var fnhtml = fnget(obj);
                    html += "<td>" + fnhtml + "</td>";
                } else {
                    var prop = $(this).data("property");
                    var propArray = prop.split(",");
                    var subHtml = "";
                    $(propArray).each(function () {
                        prop = this.trim();
                        //Seteo de datos si se desea pasar un objeto hijo desde el servidor
                        if (prop !== "" && prop !== " " && prop !== "-" && prop !== "/") {
                            var objAux = obj;
                            var array = prop.split('.');
                            for (var i = 0; i < array.length; i++) {
                                objAux = $(objAux).attr(array[i]);
                            }
                            if (objAux === null) {
                                objAux = "";
                            }
                        }else{
                            objAux = prop;
                        }
                        subHtml += objAux
                    });
                    html += "<td>" + subHtml + "</td>";
                    //html += "<td>" + $(obj).attr(prop) + "</td>";
                }
            });

            html += "<td>";

            if (bedit) {
                html += "<a alt='Editar' title='Editar' href='javaScript::void(0)' onclick='" + fnView + "(this)' data-ep='" + endpoint + "' data-rs='" + obj.id + "' data-url='?c=" + controller + "&a=viewDetail&acc=" + obj.id + "' class='btnEditar'>editar</a>";
            }
            /*if (bdetail) {
             html += "<a alt='Detalle' title='Detalle' href='javaScript::void(0)' data-ep='" + endpoint + "' data-rs='" + obj.id + "' onclick='" + ckdetail + "(this)' class='btnDetalle'>detalle</a>";
             }*/
            if (bdelete) {
                endpoint = endpoint.replace("actionList", "actionDelete");
                html += "<a alt='Eliminar' title='Eliminar' href='javaScript::void(0)' data-ep='" + endpoint + "' data-rs='" + obj.id + "' onclick='deleteResource(this)' class='btnEliminar'>eliminar</a>";
            }

            html += "</td>";
            html += "</tr>";
        });

        $(target_).html(html);
    }

    if (parseInt(r.status) !== 200) {
        createMessage(r);
}
}

function loadDataList() {
    $('*[data-eprole="list"]').each(function () {
        loadDataOneList(this);
    });
}

function loadDataFilter() {
    $('*[data-eprole="filter"]').each(function () {
        $(this).unbind();
        $(this).submit(function () {
            var parent = this.parentElement;
            var filter = $($(this).find("[type='search']")[0]).val();
            $(parent).data("filter", filter);
            loadDataOneList(parent);
            return false;
        });
    });
}

function loadDataOneList(list) {
    var deprole = list;
    var endpoint = $(deprole).data("endpoint");
    var pag = $(deprole).data("pag");
    if (!pag)
        pag = 1;
    var filter = $(deprole).data("filter");
    if (!filter)
        filter = "";
    if (endpoint) {
        httpRestFulClient(endpoint + "&p=" + pag + "&search=" + filter, null, "GET", function (r) {
            loadDataTable(deprole, r, endpoint);
            loadPaginator(deprole, r.paginator);
        });
    } else {
        alert("No se ha definido el endpoint de la lista!");
    }
}

function loadPaginator(parent_target, paginator) {
    var pag = $(parent_target).data("paginator");
    if (pag) {
        if (paginator !== null) {
            var target = $(parent_target).find("nav");
            if (target.size() > 0) {
                console.log(target);
                $(target).html(paginator);
                $(target).find("li a").each(function (e) {
                    $(this).click(function () {
                        var pag = $(this).attr("href");
                        pag = pag.replace("#", "");
                        parent_target = this.parentElement.parentElement.parentElement.parentElement;
                        $(parent_target).data("pag", pag);
                        loadDataOneList(parent_target);
                    });
                });
            } else {
                console.error("No se encuentra zona paginador");
            }
        } else {
            console.error("No esta el componente paginador");
        }
    } else {
        console.error("No esta habilitado el paginador");
    }
}

function createLoadMask(){
    var mask = $("<div id='mask'></div>");
    var loading = $("<div class='loading alert alert-warning' id=''>\n\
<p><strong>En proceso...</strong> \n\
Espere un momento, <br>la información se esta cargando!\n\
\n </p><div class='loadIcon'></div>\
</div>");
    $(mask).append(loading);
    $("body").append(mask);
}

$(function () {

    loadDataList();
    loadDataForm();

    $("form").submit(function (e) {
        var form = this;
        if (validarFormulario(this)) {
            var nosend = $(this).attr("data-nosend");
            if (!nosend) {
                var enctype = $(this).attr("enctype");
                if (enctype) {
                    return true;
                }
                var resource = $(this).attr("action");
                var data = formToObject(this);
                var method = $(this).attr("method");
                if (method !== undefined) {
                    var log = {
                        "action": resource,
                        "method": method,
                        "data": data
                    };
                    console.log("log::", log);
                    httpRestFulClient(resource, data, method, function (r) {
                        console.log("result::", r);
                        createMessage(r);
                        if (parseInt(r.status) === 200) {
                            var strFnDs = $(form).data("source");
                            if (strFnDs) {
                                var fn = eval(strFnDs);
                                fn(r.data);
                            }
                            var strckSuccess = $(form).data("success");
                            if (strckSuccess) {
                                var fn = eval(strckSuccess);
                                fn(r, form);
                            }
                            $(form).find(".val").removeClass("val");
                            if ($(form).data("reset")) {
                                form.reset();
                            }
                        }
                    });
                } else {
                    var r = {"status": 500, "message": "Debe definir el metodo de acceso."};
                    createMessage(r);
                }
            } else {
                var cbk = $(this).attr("data-fnnosend");
                var fn = eval(cbk);
                if (fn) {
                    fn();
                } else {
                    alert("No existe funcion callback para formulario tipo NoSend");
                }
            }
        }
        return false;
    });

    $('form').bind('reset', function () {
        $(this).find(".val").removeClass("val");
    });

    $("button[type=submit]").click(function (e) {
        e.preventDefault();
        var dataForm = $(e.target).data("form");
        if (dataForm === undefined) {
            alert("No se ha especificado el data-form");
        } else {
            var f = $("#" + dataForm)[0];
            addClass(f);

            //***Funcion de valudacion adicional a la general
            var addValidate = $(f).data("validate");
            var fnVal = null;
            var bool = true;
            if (addValidate !== null) {
                fnVal = eval(addValidate);
                try {
                    bool = fnVal();
                } catch (e) {
                }
            }

            if (f.reportValidity() && bool) {
                $(f).submit();
            } else {
                var r = {"status": 500, "message": "Diligencie todos los campos requeridos, <br>según el formato especificado!"};
                createMessage(r);
            }
        }
    });

    loadDataFilter();
    createLoadMask();
});