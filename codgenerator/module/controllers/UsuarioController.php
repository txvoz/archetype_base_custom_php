<?php
class UsuarioController implements IController, IManagementForm, IAction  {
private $path = "rendered/usuario/";private $config = null;public function __construct() {
        $this->config = Config::singleton();
        require "{$this->config->get("entities")}Usuario.php";
        require "{$this->config->get("models")}UsuarioModel.php";
        require "{$this->config->get("entities")}Rol.php";require "{$this->config->get("models")}RolModel.php";}public function index() {
        $this->viewList();
        }public function viewCreate() {
        $vars = [];
        $vars["create"] = true;
        $model1 = new RolModel();$vars['rols'] = $model1->get()->data;
        View::show("{$this->path}viewForm", $vars);
        }public function viewDetail() {
        $vars = [];
        $vars["id"] = $_REQUEST["acc"];
        $model1 = new RolModel();$vars['rols'] = $model1->get()->data;
        View::show("{$this->path}viewForm", $vars);
        }public function viewList() {
        View::show("{$this->path}viewList");
        }public function actionList() {
        $arg = new stdClass();
        //*******************
        $m = new UsuarioModel();
        $arg->filtro = @$_REQUEST['search'];
        $arg->paginator = null;
        $rcount = $m->getCount($arg->filtro)->cantidad;
        $arg->paginator = new Paginator($rcount, @$_REQUEST['p']);
        $r = $m->get($arg, false);
        $m->lazyLoad($r->data);
        if ($arg->paginator !== null) {
            $r->paginator = $arg->paginator->renderPaginator();
        }
        $r->count = $rcount;
        echo json_encode($r);
        }public function actionListData() {
        //*******************
        $m = new UsuarioModel();
        $rcount = $m->getCount()->cantidad;
        $r = $m->get();
        $r->count = $rcount;
        echo json_encode($r);
        }public function actionDetail() {
            if ($_SERVER['REQUEST_METHOD'] === "GET") {
                $r = null;
                $m = new UsuarioModel();
                $e = new Usuario();
                $e->setUsuId($_REQUEST["acc"]);
                $r = $m->getById($e);
                echo json_encode($r);
            } else if ($_SERVER['REQUEST_METHOD'] === "POST") {
                $this->actionUpdate();
            }
        }public function actionCreate() {
        $data = Utils::getParamsByBody();
        $e = new Usuario();
        $e->serializeByObject($data);
        $m = new UsuarioModel();
        $r = $m->insert($e);
        echo json_encode($r);
        }public function actionDelete() {
        $m = new UsuarioModel();
        $e = new Usuario();
        $e->setUsuId($_REQUEST["acc"]);
        $r = $m->delete($e);
        echo json_encode($r);
        }public function actionUpdate() {
        $data = Utils::getParamsByBody();
        $e = new Usuario();
        $e->serializeByObject($data);
        $m = new UsuarioModel();
        $r = $m->update($e);
        echo json_encode($r);
        }
}