<?php
class Rol extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $rolId;
	protected $rolNombre;
/* Getters */
	public function getRolId(){
		return $this->rolId;
	}

	public function getRolNombre(){
		return $this->rolNombre;
	}

	public function getId(){
		return $this->getRolId();
	}

/* Setters */
	public function setRolId($param){
$this->setId($param);
		$this->rolId = $param;
	}

	public function setRolNombre($param){
		$this->rolNombre = $param;
	}

public function jsonSerialize() {
        $this->id = $this->rolId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        }
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}