<?php
class UsuarioModel extends BasicModel implements IModel {
//***** Attributes *****//
	private $conexion;
	private $table = 'usuario';
	private $nameEntity = 'Usuario';
	private $filter = 'usuCorreo';
//***** Constructs *****//
	public function __construct(){
		 $this->conexion = MyPDO::singleton();
	}

//***** Methods *****//
public function getCount($obj = null) {
    	$retorno = new stdClass();
    try {
      //****** Paginacion y Filtros
             $where = '';
            if ($obj != '') {
                $where = " where {$this->filter} like '%{$obj}%' ";
            }
            //*****
      		$sql = "SELECT count(*) as cantidad FROM {$this->table} {$where} ";
      		$query = $this->conexion->prepare($sql);
      		$query->execute();
      		$r = $query->fetchObject();
      		$retorno->cantidad = $r->cantidad;
    } catch (PDOException $e) {
      		$retorno->status = 501;
      		$retorno->message = Utils::simpleCatchMessage($e->getMessage());
    }
    	return $retorno;
  }	public function get($obj=null,$all=true){
	$retorno = new stdClass()
;
    	try {


    //****** Paginacion y Filtros
            $where = '';
            $limit = '';
            if ($obj !== null) {
                if ($obj->filtro != '') {
                    $where = " where {$this->filter} like '%{$obj->filtro}%' ";
                }
                if (!$all) {
                    $paginacion = $obj->paginator;
                    $paginacion instanceof Paginator;
                    $c = Paginator::$COUNT_BY_PAG;
                    $i = $paginacion->getInicioLimit();
                    $limit = "limit {$i},{$c}";
                }
            }
            //*****

      //*****
      $sql = "select * from {$this->table} {$where} order by {$this->filter} asc {$limit}";

      		$query = $this->conexion->prepare($sql);
      		$query->execute();
      		$retorno->data = $query->fetchAll(PDO::FETCH_CLASS, $this->nameEntity);
      		$retorno->status = 200;
      		$retorno->message = "Consulta exitosa";
      		if (count($retorno->data) === 0) {
        		$retorno->status = 201;
        		$retorno->message = "No hay registros en la base de datos.";
      		}
    	} catch (PDOException $e) {
      		$retorno->message = Utils::simpleCatchMessage($e->getMessage());
      		$retorno->status = 501;
    	}
    //$this->lazyLoad($retorno->data);
    	return $retorno;
	}

	public function getById($entity){
$retorno = new stdClass();
    try {
      $entity instanceof Usuario;
      $sql = "select * from {$this->table} where usuId = ?";
      $query = $this->conexion->prepare($sql);
      @$query->bindParam(1, $entity->getUsuId());
      $query->execute();
      $retorno->data = $query->fetchObject($this->nameEntity);
      $retorno->status = 200;
      $retorno->message = "{$this->nameEntity} ncontrado";
      if (!$retorno->data instanceof $this->nameEntity) {
        $retorno->status = 201;
        $retorno->message = "{$this->nameEntity} no encontrado";
      }
    } catch (PDOException $e) {
      $retorno->message = Utils::simpleCatchMessage($e->getMessage());
      $retorno->status = 501;
    }
    //$this->lazyLoad($retorno->data);
    return $retorno;
	}

	public function insert($entity){
$retorno = new stdClass();
    try {
      $entity instanceof Usuario; 
$sql = "insert into {$this->table} values ( null,?,?,?)";
$query = $this->conexion->prepare($sql);	@$query->bindParam(1, $entity->getUsuCorreo());	@$query->bindParam(2, $entity->getUsuPassword());	@$query->bindParam(3, $entity->getRolId());
      $query->execute();
      $id = $this->conexion->lastInsertId();
      $entity->setUsuId($id);
      $retorno->data = $entity;
      $retorno->status = 200;
      $retorno->message = "Registro insertado.";
    } catch (PDOException $e) {
      $retorno->message = Utils::simpleCatchMessage($e->getMessage());
      $retorno->status = 501;
    }
    return $retorno;
	}

	public function update($entity){
$retorno = new stdClass();
    try {
      $entity instanceof Usuario;
      $sql = "update {$this->table} set " 
."usuCorreo = ?,"
."usuPassword = ?,"
."rolId = ? "
. "where usuId=?"; 
      $query = $this->conexion->prepare($sql); 	@$query->bindParam(1, $entity->getUsuCorreo());	@$query->bindParam(2, $entity->getUsuPassword());	@$query->bindParam(3, $entity->getRolId());	@$query->bindParam(4, $entity->getUsuId());
      $query->execute();
      $retorno->data = $entity;
      $retorno->status = 200;
      $retorno->message = "Registro Actualizado.";
    } catch (PDOException $e) {
      $retorno->message = Utils::simpleCatchMessage($e->getMessage());
      $retorno->status = 501;
    }
    return $retorno;	}

	public function delete($entity){
$retorno = new stdClass();
    try {
      $entity instanceof Usuario;
      $sql = "delete from {$this->table} where usuId = ?";
      $query = $this->conexion->prepare($sql);
      @$query->bindParam(1, $entity->getUsuId());
      $query->execute();
      $retorno->status = 200;
      $retorno->message = "{$this->nameEntity} eliminado";
    } catch (PDOException $e) {
      $retorno->message = Utils::simpleCatchMessage($e->getMessage());
      $retorno->status = 501;
    }
    return $retorno;	}


}