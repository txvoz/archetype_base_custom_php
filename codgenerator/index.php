<?php
//Incluimos algunas clases:
require_once '../appserver/_core/class_/Config.php'; //de configuracion
require_once $c->get('dep_bd') . "MyPDO.php"; //PDO con singleton
require_once 'class_/Precode.php';
require_once 'class_/CodeGenerator.php';

$cg = CodeGenerator::singleton();
$cg->generateAll();
