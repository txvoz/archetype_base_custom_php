<?php

$GLOBALS["ref_table"] = "";
$GLOBALS["primary"] = "";

//Incluimos algunas clases:
require_once '../appserver/_core/class_/Config.php'; //de configuracion
require_once $c->get('dep_bd') . "MyPDO.php"; //PDO con singleton
require_once 'Precode.php';

$nameCls = "";
$dirModule = null;
$primary = "";

function createEntity($obj, $r, $ra, $nameCls, $dirModule) {


//Entity
    $class = "<?php\nclass {$nameCls} extends BasicEntity implements JsonSerializable {\n";

//Atributos
    $class .= "/* Attributes */\n";
    foreach ($ra->data as $obj2) {
        print_r($obj2);
        if ($obj2->Key === "PRI") {
            $primary = $obj2->Field;
            $GLOBALS["primary"] = $primary;
            $class .= "//@PrimaryKey\n";
        } else if ($obj2->Key === "MUL") {
            $class .= "//@Index\n";
        }
        $class .= "\tprivate \${$obj2->Field};\n";
    }

//Getter
    $class .= "/* Getters */\n";
    foreach ($ra->data as $obj2) {
        $name = ucfirst($obj2->Field);
        $class .= "\tpublic function get{$name}(){\n";
        $class .= "\t\treturn \$this->{$obj2->Field};\n";
        $class .= "\t}\n\n";
    }


    $name = ucfirst($primary);
    $class .= "\tpublic function getId(){\n";
    $class .= "\t\treturn \$this->get{$name}();\n";
    $class .= "\t}\n\n";

//Setter
    $class .= "/* Setters */\n";
    foreach ($ra->data as $obj2) {
        $name = ucfirst($obj2->Field);
        $class .= "\tpublic function set{$name}(\$param){\n";
        if ($obj2->Key === "PRI") {
            $class .= "\$this->setId(\$param);\n";
        }
        $class .= "\t\t\$this->{$obj2->Field} = \$param;\n";
        $class .= "\t}\n\n";
    }

    $class .= "public function jsonSerialize() {
        \$this->id = \$this->{$primary};
        return get_object_vars(\$this);
    }";

    $class .= "\n}";

    $file = fopen("{$dirModule}entities/{$nameCls}.php", "w+");
    fwrite($file, $class);
    fclose($file);
    @chmod($file, 0777);
}

function createModel($obj, $r, $ra, $nameCls, $dirModule) {
    $ref_table = $GLOBALS["ref_table"];
    $primary = $GLOBALS["primary"];
//Model
    $class = "<?php\nclass {$nameCls}Model implements IModel {\n";
//Atributos
    $class .= "//***** Attributes *****//\n";
    $class .= "\tprivate \$conexion;\n";
    @$class .= "\tprivate \$table = '{$obj->{$ref_table}}';\n";
    $class .= "\tprivate \$nameEntity = '{$nameCls}';\n";
    $class .= "\tprivate \$filter = '{$ra->data[1]->Field}';\n";
//Contructs
    $class .= "//***** Constructs *****//\n";
    $class .= "\tpublic function __construct(){\n";
    $class .= "\t\t \$this->conexion = MyPDO::singleton();\n";
    $class .= "\t}\n\n";
//Methods
    $class .= "//***** Methods *****//\n";

    $class .= "public function getCount(\$obj = null) {
    \t\$retorno = new stdClass();
    try {
      \t\t\$where = \"\";
      if(\$obj!=\"\"){
        \$where=\" where {\$this->filter} like '%{\$obj}%' \";
      }
      \t\t\$sql = \"SELECT count(*) as cantidad FROM {\$this->table} {\$where} \";
      \t\t\$query = \$this->conexion->prepare(\$sql);
      \t\t\$query->execute();
      \t\t\$r = \$query->fetchObject();
      \t\t\$retorno->cantidad = \$r->cantidad;
    } catch (PDOException \$e) {
      \t\t\$retorno->status = 501;
      \t\t\$retorno->msg = \$e->getMessage();
    }
    \treturn \$retorno;
  }";

    $class .= "\tpublic function get(\$entity=null){\n";
    $class .= "\t\$retorno = new stdClass()\n;
    \ttry {\n
      \t\t\$where = \"\";
      \t\tif(\$obj->filtro!=\"\"){
        \t\t\t\$where=\" where {\$this->filter} like '%{\$obj->filtro}%' \";
      \t\t}
      \t\t\$paginacion = \$obj->paginacion;
      \t\t\$paginacion instanceof Paginacion;
      \t\t\$c = Paginacion::\$cantidadRegistros;
      \t\t\$i = \$paginacion->getInicioLimit();
      //*****
      \t\t\$sql = \"select * from {\$this->table} {\$where} order by {\$this->filter} asc limit {\$i},{\$c}\";
      \t\t\$query = \$this->conexion->prepare(\$sql);
      \t\t\$query->execute();
      \t\t\$retorno->data = \$query->fetchAll(PDO::FETCH_CLASS, \$this->nameEntity);
      \t\t\$retorno->status = 200;
      \t\t\$retorno->msg = \"Consulta exitosa\";
      \t\tif (count(\$retorno->data) === 0) {
        \t\t\$retorno->status = 201;
        \t\t\$retorno->msg = \"No hay registros en la base de datos.\";
      \t\t}
    \t} catch (PDOException \$e) {
      \t\t\$retorno->msg = \$e->getMessage();
      \t\t\$retorno->status = 501;
    \t}
    \treturn \$retorno;\n";

    $class .= "\t}\n\n";

    $namePRI = ucfirst(@$primary);
    $class .= "\tpublic function getById(\$entity){\n";
    @$class .= "\$retorno = new stdClass();
    try {
      \$obj instanceof {$nameCls};
      \$sql = \"select * from {\$this->table} where {$primary} = ?\";
      \$query = \$this->conexion->prepare(\$sql);
      @\$query->bindParam(1, \$obj->get{$namePRI}());
      \$query->execute();
      \$retorno->data = \$query->fetchObject(\$this->nameEntity);
      \$retorno->status = 200;
      \$retorno->msg = \"{\$this->nameEntity} \encontrado\";
      if (!\$retorno->data instanceof \$this->nameEntity) {
        \$retorno->status = 201;
        \$retorno->msg = \"{\$this->nameEntity} no encontrado\";
      }
    } catch (PDOException \$e) {
      \$retorno->msg = \$e->getMessage();
      \$retorno->status = 501;
    }
    return \$retorno;\n";
    $class .= "\t}\n\n";

    $class .= "\tpublic function insert(\$entity){\n";
    $class .= "\$retorno = new stdClass();
    try {
      \$obj instanceof {$nameCls}; \n";

    $countAtt = count($ra->data);

    $class .= "\$sql = \"insert into {\$this->table} values ( null,";
    $i = 1;
    foreach ($ra->data as $obj2) {
        if ($obj2->Field !== @$primary) {
            $class .= "?";
            if ($i < ($countAtt - 1)) {
                $class .= ",";
            }
            $i++;
        }
    }
    $class .= ")\";\n";
    $class .= "\$query = \$this->conexion->prepare(\$sql);";
    $i = 1;
    foreach ($ra->data as $obj2) {
        if ($obj2->Field !== @$primary) {
            $name = ucfirst($obj2->Field);
            $class .= "\t@\$query->bindParam({$i}, \$obj->get{$name}());";
            $i++;
        }
    }
    @$class .= "
      \$query->execute();
      \$id = \$this->conexion->lastInsertId();
      \$obj->set{$namePRI}(\$id);
      \$retorno->data = \$obj;
      \$retorno->status = 200;
      \$retorno->msg = \"Registro insertado.\";
    } catch (PDOException \$e) {
      \$retorno->msg = \$e->getMessage();
      \$retorno->status = 501;
    }
    return \$retorno;\n";
    $class .= "\t}\n\n";

    $class .= "\tpublic function update(\$entity){\n";
    $class .= "\$retorno = new stdClass();
    try {
      \$obj instanceof {$nameCls};";

    $class .= "
      \$sql = \"update {\$this->table} set \" \n";

    $i = 1;
    foreach ($ra->data as $obj2) {
        if ($obj2->Field !== @$primary) {
            $class .= ".\"{$obj2->Field} = ?";
            if ($i < ($countAtt - 1)) {
                $class .= ",\"\n";
            } else {
                $class .= " \"\n";
            }
            $i++;
        }
    }

    @$class .= ". \"where {$primary}=?\"; ";

    $class .= "
      \$query = \$this->conexion->prepare(\$sql); ";

    $i = 1;
    foreach ($ra->data as $obj2) {
        if ($obj2->Field !== @$primary) {
            $name = ucfirst($obj2->Field);
            $class .= "\t@\$query->bindParam({$i}, \$obj->get{$name}());";
            $i++;
        }
    }

    $class .= "\t@\$query->bindParam({$i}, \$obj->get{$namePRI}());";

    $class .= "
      \$query->execute();
      \$retorno->data = \$obj;
      \$retorno->status = 200;
      \$retorno->msg = \"Registro Actualizado.\";
    } catch (PDOException \$e) {
      \$retorno->msg = \$e->getMessage();
      \$retorno->status = 501;
    }
    return \$retorno;";
    $class .= "\t}\n\n";

    $class .= "\tpublic function delete(\$entity){\n";
    @$class .= "\$retorno = new stdClass();
    try {
      \$obj instanceof {$nameCls};
      \$sql = \"delete from {\$this->table} where {$primary} = ?\";
      \$query = \$this->conexion->prepare(\$sql);
      @\$query->bindParam(1, \$obj->get{$namePRI}());
      \$query->execute();
      \$retorno->status = 200;
      \$retorno->msg = \"{\$this->nameEntity} eliminado\";
    } catch (PDOException \$e) {
      \$retorno->msg = \$e->getMessage();
      \$retorno->status = 501;
    }
    return \$retorno;";
    $class .= "\t}\n\n";

    $class .= "\n}";

    $file = fopen("{$dirModule}models/{$nameCls}Model.php", "w+");
    fwrite($file, $class);
    fclose($file);
    @chmod($file, 0777);
}

function createController($obj, $r, $ra, $nameCls, $dirModule) {
    $ref_table = $GLOBALS["ref_table"];
    $primary = $GLOBALS["primary"];
    $namePri = strtolower($primary);
    //******
    $namecls = strtolower($nameCls);
//Controller
    $class = "<?php\nclass {$nameCls}Controller implements IController {\n";
//Atributos
    $class .= "//***** Attributes *****//\n";
    $class .= "\tprivate \$config;\n";
    $class .= "\tprivate \$model;\n";
    $class .= "\tprivate \$path = \"public/{$namecls}/\";\n";
//Contructs
    $class .= "//***** Constructs *****//\n";
    $class .= "public function __construct() {
    \$this->view = new View();
    \$this->config = Config::singleton();
    require_once \$this->config->get(\"entities\") . \"{$nameCls}.php\";
    require_once \$this->config->get(\"models\") . \"{$nameCls}Model.php\";
    \$this->model = new {$nameCls}Model();
  }\n\n";
//Methods
    $class .= "//***** Methods *****//\n";
    $class .= "
    public function index() {
      \$count = \$this->model->getCount(@\$_REQUEST[\"filtro\"]);

      /*\$o = new stdClass();
      \$o->paginacion = new Paginacion(\$count->cantidad);
      \$o->paginacion->setPaginaActual(@\$_REQUEST[\"pp\"]);
      \$o->filtro = @\$_REQUEST[\"filtro\"];*/

      \$r = \$m->get(\$o);
      //*******
      \$vars[\"data\"] = \$r->data;
      \$vars[\"msg\"] = @\$_REQUEST[\"msg\"];
      \$vars[\"status\"] = @\$_REQUEST[\"status\"];
      //\$vars[\"pp\"] = \$o->paginacion;
      //\$vars[\"filtro\"] = \$o->filtro;
      \$this->view->show(\"{\$this->path}lista\",\$vars);
    }\n\n";
    /*
      public function index() {
      //*******
      \$m = new {$nameCls}Model();
      \$r = \$m->get();
      //*******
      \$vars[\"data\"] = \$r->data;
      \$vars[\"msg\"] = @\$_REQUEST[\"msg\"];
      \$vars[\"status\"] = @\$_REQUEST[\"status\"];
      \$this->view->show(\"{\$this->path}lista.php\",\$vars);
      }\n\n"; */

    $class .= "public function frmEditar() {
    //*******
    \$m = new {$nameCls}Model();
    \$o = new {$nameCls}();
    \$o->set{$namePri}(\$_REQUEST[\"acc\"]);
    \$r = \$m->getById(\$o);
    //*******
    \$vars[\"entity\"] = \$r->data;
    \$this->view->show(\"{\$this->path}form_edit\",\$vars);
   }\n\n";

    $class .= "public function frmRegistro() {
    \$this->view->show(\"{\$this->path}form\");
   }\n\n";

    @$class .= "public function editar() {
    //*******
    \$m = new {$nameCls}Model();
    \$entity = new {$nameCls}();
    \$entity->set{$namePri}(\$_REQUEST[\"{$primary}\"]);";

    $i = 1;
    foreach ($ra->data as $obj2) {
        if ($obj2->Field !== @$primary) {
            $name = ucfirst($obj2->Field);
            $class .= "\$entity->set{$name}(\$_REQUEST[\"{$obj2->Field}\"]);\n";
            $i++;
        }
    }

    $class .= "\$r = \$m->update(\$entity);
    header(\"location: ?c={$nameCls}&msg={\$r->msg}&status={\$r->status}\");
   }\n\n";

    $class .= "public function registro() {
    //*******
    \$m = new {$nameCls}Model();
    \$entity = new {$nameCls}();";

    $i = 1;
    foreach ($ra->data as $obj2) {
        if ($obj2->Field !== @$primary) {
            $name = ucfirst($obj2->Field);
            $class .= "\$entity->set{$name}(\$_REQUEST[\"{$obj2->Field}\"]);\n";
            $i++;
        }
    }

    $class .= "\$r = \$m->insert(\$entity);
  header(\"location: ?c={$nameCls}&msg={\$r->msg}&status={\$r->status}\");
   }\n\n";

    $primaryPri = ucfirst(@$primary);
    $class .= "public function eliminar() {
    //*******
    \$m = new {$nameCls}Model();
    \$entity = new {$nameCls}();
    \$entity->set{$primaryPri}(\$_REQUEST[\"acc\"]);
    \$r = \$m->delete(\$entity);
    //*******
    header(\"location: ?c={$nameCls}&msg={\$r->msg}&status={\$r->status}\");
   }\n\n";

    $class .= "}";
    $file = fopen("{$dirModule}controllers/{$nameCls}Controller.php", "w+");
    fwrite($file, $class);
    fclose($file);
    @chmod($file, 0777);
}

function createViewList($obj, $r, $ra, $nameCls, $dirModule) {
    $ref_table = $GLOBALS["ref_table"];
    $primary = $GLOBALS["primary"];
    //******
    $html = "
<?php
\$config = Config::singleton();
//
\$controller = isset(\$_REQUEST[\"c\"])?\"c={\$_REQUEST[\"c\"]}&\":\"\";
\$action = isset(\$_REQUEST[\"a\"])?\"a={\$_REQUEST[\"a\"]}&\":\"\";
\$path_b = \"?{\$controller}{\$action}\";
?>
<!DOCTYPE html>
<html>
  <head>
    <?php
    require_once \$config->get(\"views\") . \"templates/meta_header.php\";
    ?>
  </head>
  <body>
    <div id=\"content_root\">
      <?php
      require_once \$config->get(\"views\") . \"templates/header_private.php\";
      require_once \$config->get(\"views\") . \"templates/nav_horizontal_funcionario.php\";
      ?>
      <div class=\"content_principal\">
        <?php 
        require_once \$config->get(\"views\") . \"templates/msg.php\";
        ?>
        <h2>Lista de {$nameCls}</h2>
        <form action='<?php echo \$path_b ?>' method='post'>
          <input type='search' placeholder='Buscar...' name='filtro' value='<?php echo \$filtro ?>' focus />
          <button>Buscar</button>
        </form>
        <div class='paginacion'>
          <?php
          \$navegacion = \$pp->getNavegacion();
          \$_filtro = \$filtro!=\"\"?\"&filtro={\$filtro}\":\"\";
          foreach (\$navegacion as \$key => \$item) {
            if (\$item->status) {
              \$path_paginacion = \"{\$path_b}pp={\$item->data}{\$_filtro}\";
              ?>
              <div><a href=\"<?php echo \$path_paginacion ?>\"><?php echo \$key ?></a></div>
              <?php
            } else {
              if(\$key!=='<' && \$key!=='>')
                echo \"<div class='activo'>{\$key}</div>\";
            }
          }
          ?>
        </div>
        <table>
          <thead>
            <tr> ";

    foreach ($ra->data as $obj2) {
        $html .= "<th>{$obj2->Field}</th>\n";
    }

    $html .= "<th></th> 
             </tr>
          </thead>
          <tbody>
            <?php
            foreach (\$data as \$entity) {
              ?>
              <tr>";

    foreach ($ra->data as $obj2) {
        $upper = ucfirst($obj2->Field);
        $html .= "<td><?php echo \$entity->get{$upper}() ?></td>\n";
    }

    $html .= "<td>
                  <a href=\"?c={$nameCls}&a=frmEditar&acc=<?php echo \$entity->getId() ?>\"><div class=\"icon icon20 editar\"></div></a>
                  <a href=\"?c={$nameCls}&a=eliminar&acc=<?php echo \$entity->getId() ?>\"><div class=\"icon icon20 eliminar\"></div></a>
                </td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
        <div class='paginacion'>
          <?php
          \$navegacion = \$pp->getNavegacion();
          \$_filtro = \$filtro!=\"\"?\"&filtro={\$filtro}\":\"\";
          foreach (\$navegacion as \$key => \$item) {
            if (\$item->status) {
              \$path_paginacion = \"{\$path_b}pp={\$item->data}{\$_filtro}\";
              ?>
              <div><a href=\"<?php echo \$path_paginacion ?>\"><?php echo \$key ?></a></div>
              <?php
            } else {
              if(\$key!=='<' && \$key!=='>')
                echo \"<div class='activo'>{\$key}</div>\";
            }
          }
          ?>
        </div>
      </div>
      <?php
      require_once \$config->get(\"views\") . \"templates/footer_auth.php\";
      ?>
    </div>
  </body>
</html>
";

    $file = fopen("{$dirModule}/lista.php", "w+");
    fwrite($file, $html);
    fclose($file);
    @chmod($file, 0777);
}

function createForm($obj, $r, $ra, $nameCls, $dirModule) {
    $ref_table = $GLOBALS["ref_table"];
    $primary = $GLOBALS["primary"];
    //******
    $html = "<?php
\$config = Config::singleton();
?>
<!DOCTYPE html>
<html>
  <head>
    <?php
    require_once \$config->get(\"views\") . \"templates/meta_header.php\";
    ?>
  </head>
  <body>
    <div id=\"content_root\">
      <?php
      require_once \$config->get(\"views\") . \"templates/header_private.php\";
      require_once \$config->get(\"views\") . \"templates/nav_horizontal_funcionario.php\";
      ?>
      <div class=\"content_principal\">

        <section class=\"col\" id=\"col2\">
          <form action=\"?c={$nameCls}&a=registro\" id=\"{$nameCls}\" method=\"post\" >
            <h1 class=\"title\">Registro de {$nameCls}</h1>
            ";

    foreach ($ra->data as $obj2) {
        $upper = ucfirst($obj2->Field);
        $html .= "\n<!-- Key {$obj2->Key} / Type {$obj2->Type} -->\n";
        $html .= "<label>{$obj2->Field} {$nameCls}</label>\n";
        $required = "";
        if ($obj2->Null === "NO") {
            $required = "required";
        }
        if (strpos($obj2->Type, "int") !== false) {
            $html .= "<input value = \"\" type=\"number\" name=\"{$obj2->Field}\" id=\"{$obj2->Field}\" {$required} /><br>";
        } else if (strpos($obj2->Type, "varchar") !== false) {
            $html .= "<input value = \"\" type=\"text\" name=\"{$obj2->Field}\" id=\"{$obj2->Field}\" {$required} /><br>";
        } else if (strpos($obj2->Type, "date") !== false) {
            $html .= "<input value = \"\" type=\"date\" name=\"{$obj2->Field}\" id=\"{$obj2->Field}\" {$required} /><br>";
        } else if (strpos($obj2->Type, "enum") !== false) {
            $strTipo = str_replace("enum", "", $obj2->Type);
            $strTipo = str_replace("(", "[", $strTipo);
            $strTipo = str_replace(")", "]", $strTipo);
            $strTipo = str_replace("'", "\"", $strTipo);
            $dataEnum = json_decode($strTipo);
            $html .= "<select name=\"{$obj2->Field}\" id=\"{$obj2->Field}\" {$required} >\n";
            foreach ($dataEnum as $key => $value) {
                $html .= "<option value='$value'>{$value}</option>\n";
            }
            $html .= "</select>\n";
        }
    }



    $html .= "
  <br>
  <button>Enviar</button>
  </form>
  </section>

  </div>
  <?php
  require_once \$config->get(\"views\") . \"templates/footer_auth.php\";
      ?>
    </div>
  </body>
</html>
";

    $file = fopen("{$dirModule}/form.php", "w+");
    fwrite($file, $html);
    fclose($file);
    @chmod($file, 0777);
}

function createFormEdit($obj, $r, $ra, $nameCls, $dirModule) {
    $ref_table = $GLOBALS["ref_table"];
    $primary = $GLOBALS["primary"];
    //******
    $html = "<?php
\$config = Config::singleton();
?>
<!DOCTYPE html>
<html>
  <head>
    <?php
    require_once \$config->get(\"views\") . \"templates/meta_header.php\";
    ?>
  </head>
  <body>
    <div id=\"content_root\">
      <?php
      require_once \$config->get(\"views\") . \"templates/header_private.php\";
      require_once \$config->get(\"views\") . \"templates/nav_horizontal_funcionario.php\";
      ?>
      <div class=\"content_principal\">

        <section class=\"col\" id=\"col2\">
          <form action=\"?c={$nameCls}&a=editar\" id=\"{$nameCls}\" action=\"\" method=\"post\" >
            <h1 class=\"title\">Actualización de {$nameCls}</h1>
            ";

            foreach ($ra->data as $obj2) {
        $upper = ucfirst($obj2->Field);
        $html .= "\n<!-- Key {$obj2->Key} / Type {$obj2->Type} -->\n";
        $html .= "<label>{$obj2->Field} {$nameCls}</label>\n";
        $required = "";
        if ($obj2->Null === "NO") {
            $required = "required";
        }
        if (strpos($obj2->Type, "int") !== false) {
            $html .= "<input value = \"<?php echo \$entity->get{$upper}(); ?>\" type=\"number\" name=\"{$obj2->Field}\" id=\"{$obj2->Field}\" {$required} /><br>";
        } else if (strpos($obj2->Type, "varchar") !== false) {
            $html .= "<input value = \"<?php echo \$entity->get{$upper}(); ?>\" type=\"text\" name=\"{$obj2->Field}\" id=\"{$obj2->Field}\" {$required} /><br>";
        } else if (strpos($obj2->Type, "date") !== false) {
            $html .= "<input value = \"<?php echo \$entity->get{$upper}(); ?>\" type=\"date\" name=\"{$obj2->Field}\" id=\"{$obj2->Field}\" {$required} /><br>";
        } else if (strpos($obj2->Type, "enum") !== false) {
            $strTipo = str_replace("enum", "", $obj2->Type);
            $strTipo = str_replace("(", "[", $strTipo);
            $strTipo = str_replace(")", "]", $strTipo);
            $strTipo = str_replace("'", "\"", $strTipo);
            $dataEnum = json_decode($strTipo);
            $html .= "<select name=\"{$obj2->Field}\" id=\"{$obj2->Field}\" {$required} >\n";
            foreach ($dataEnum as $key => $value) {
                $html .= "<option value='$value'>{$value}</option>\n";
            }
            $html .= "</select>\n";
        }
    }
    
    $html .= "
  <br>
  <button>Enviar</button>
  </form>
  </section>

  </div>
  <?php
  require_once \$config->get(\"views\") . \"templates/footer_auth.php\";
      ?>
    </div>
  </body>
</html>
";

    $file = fopen("{$dirModule}/form_edit.php", "w+");
    fwrite($file, $html);
    fclose($file);
    @chmod($file, 0777);
}

function createMenu($r) {
    $ref_table = $GLOBALS["ref_table"];
    $primary = $GLOBALS["primary"];
    //******
    $html = "<ul class='menu'>";
    foreach ($r->data as $obj) {
        @$name = ucfirst($obj->{$ref_table});
        $html .= "<li>
      <a href=\"#\">{$name}</a>
        <ul class = \"submenu_horizontal\">
        <li><a href = \"?c={$name}&a=index\">Listar</a></li>
        <li><a href = \"?c={$name}&a=frmRegistro\">Registro</a></li>
        </ul>
      </li>
      ";
    }
    $html .= "</ul>";
    $file = fopen("menu.php", "w+");
    fwrite($file, $html);
    fclose($file);
    @chmod($file, 0777);
}

function create() {
    echo "<pre>";
    $p = new Precode();
    $r = $p->get();
    $tables = [];
    if (count($r->data) > 0) {
        foreach ($r->data[0] as $key => $value) {
            $ref_table = $key;
            $GLOBALS["ref_table"] = $ref_table;
            break;
        }
    }
    //******
    foreach ($r->data as $obj) {
        $nameCls = ucfirst($obj->{$ref_table});
        $dirModule = "module/views/{$obj->{$ref_table}}/";
        @mkdir($dirModule, 777);
        /* @mkdir("{$dirModule}entity/", 777);
          @mkdir("{$dirModule}controller/", 777);
          @mkdir("{$dirModule}model/", 777);
          @mkdir("{$dirModule}views/", 777); */

        echo $obj->{$ref_table} . " ===> OK <br>";
        $ra = $p->getAttributesFromTable($obj->{$ref_table});
        echo $ra->data[1]->Field . "<br>----------------------------<br>";
        //print_r($ox); //$ra->data[1]->filed."<br>----------------------------<br>";
        //print_r($ra);

        $dirModule = "module/";
        createEntity($obj, $r, $ra, $nameCls, $dirModule);
        createModel($obj, $r, $ra, $nameCls, $dirModule);
        createController($obj, $r, $ra, $nameCls, $dirModule);

        $dirModule = "module/views/{$obj->{$ref_table}}/";
        createViewList($obj, $r, $ra, $nameCls, $dirModule);
        createForm($obj, $r, $ra, $nameCls, $dirModule);
        createFormEdit($obj, $r, $ra, $nameCls, $dirModule);
        //break;*/
    }

    createMenu($r);
}

create();
