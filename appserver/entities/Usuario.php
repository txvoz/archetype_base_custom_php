<?php
class Usuario extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $usuId;
	protected $usuCorreo;
	protected $usuPassword;
/* @Index */
	protected $fkrol = null;
	protected $rolId;
/* Getters */
	public function getUsuId(){
		return $this->usuId;
	}

	public function getUsuCorreo(){
		return $this->usuCorreo;
	}

	public function getUsuPassword(){
		return $this->usuPassword;
	}

	public function getRolId(){
		return $this->rolId;
	}

/** Index **/
	public function getFkRol(){
if($this->fkrol===null){$model = new rolModel();$e = new Rol();$e->setRolId($this->rolId);$r = $model->getById($e);if($r->status===200){$this->fkrol = $model->getById($e)->data;}}		return $this->fkrol;
	}

	public function getId(){
		return $this->getUsuId();
	}

/* Setters */
	public function setUsuId($param){
$this->setId($param);
		$this->usuId = $param;
	}

	public function setUsuCorreo($param){
		$this->usuCorreo = $param;
	}

	public function setUsuPassword($param){
		$this->usuPassword = $param;
	}

	public function setRolId($param){
		$this->rolId = $param;
	}

public function jsonSerialize() {
        $this->id = $this->usuId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        $this->getFkRol();}
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}