<?php
class Prueba extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $pruId;
	protected $pruNombre;
	protected $pruFecha;
/* Getters */
	public function getPruId(){
		return $this->pruId;
	}

	public function getPruNombre(){
		return $this->pruNombre;
	}

	public function getPruFecha(){
		return $this->pruFecha;
	}

	public function getId(){
		return $this->getPruId();
	}

/* Setters */
	public function setPruId($param){
$this->setId($param);
		$this->pruId = $param;
	}

	public function setPruNombre($param){
		$this->pruNombre = $param;
	}

	public function setPruFecha($param){
		$this->pruFecha = $param;
	}

public function jsonSerialize() {
        $this->id = $this->pruId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        }
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}