
                  <?php
                  if(@$create){
                    $reset = true;
                    $action = 'Create';
                    $role = '';
                  }else{
                    $reset = false;
                    $action = 'Detail';
                    $role = "data-eprole='form'";
                  }
                  ?>
                  <form data-reset='<?php echo $reset ?>' 
                      id='frmUsuario' 
                      action='?c=usuario&a=action<?php echo $action ?>' 
                      method='POST' 
                      <?php echo $role ?>
                      ><div class='row'>
        <div class='col-sm-12'>
                <div class='form-group'>
                    <h2>Datos de Usuario</h2>
                </div>
            </div>
        </div><div class='row'><div class='col-sm-4'>
            <div class='form-group'>
                <label for='usuId'>usuId:</label><input readonly 
                    minlength='1' 
                    maxlength='100000' 
                    type='number' 
                    class='form-control' 
                    id='usuId' 
                    name='usuId' 
                     /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='usuCorreo'>usuCorreo:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='usuCorreo' 
                    name='usuCorreo' 
                    required /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='usuPassword'>usuPassword:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='usuPassword' 
                    name='usuPassword' 
                    required /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='rolId'>rolId:</label><select required class='form-control'id='rolId'name='rolId'><option value=''>[SELECCIONE OPCION]</option><?php foreach ($rols as $entity) {echo "<option value='{$entity->getId()}'>{$entity->describeStr()}</option>";} ?></select></div>
            </div></div><div class='row'>
            <div class='col-sm-12'>
                <button type='submit' 
                        class='btn btn-success' 
                        data-form='frmUsuario'>Guardar</button>
                <?php
                  if(@$create){
                  ?>
                  <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                  <?php
                  }else{
                  ?>
                  <a href='?c=usuario&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                  <?php
                  }
                  ?>
                <a href='?c=usuario' class='btn btn-warning'>Ver todos los registros</a>
                                
            </div>
            <hr class='d-sm-none'>
        </div></form>