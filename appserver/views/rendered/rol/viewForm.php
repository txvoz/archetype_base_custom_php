
                  <?php
                  if(@$create){
                    $reset = true;
                    $action = 'Create';
                    $role = '';
                  }else{
                    $reset = false;
                    $action = 'Detail';
                    $role = "data-eprole='form'";
                  }
                  ?>
                  <form data-reset='<?php echo $reset ?>' 
                      id='frmRol' 
                      action='?c=rol&a=action<?php echo $action ?>' 
                      method='POST' 
                      <?php echo $role ?>
                      ><div class='row'>
        <div class='col-sm-12'>
                <div class='form-group'>
                    <h2>Datos de Rol</h2>
                </div>
            </div>
        </div><div class='row'><div class='col-sm-4'>
            <div class='form-group'>
                <label for='rolId'>rolId:</label><input readonly 
                    minlength='1' 
                    maxlength='100000' 
                    type='number' 
                    class='form-control' 
                    id='rolId' 
                    name='rolId' 
                     /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='rolNombre'>rolNombre:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='rolNombre' 
                    name='rolNombre' 
                    required /></div>
            </div></div><div class='row'>
            <div class='col-sm-12'>
                <button type='submit' 
                        class='btn btn-success' 
                        data-form='frmRol'>Guardar</button>
                <?php
                  if(@$create){
                  ?>
                  <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                  <?php
                  }else{
                  ?>
                  <a href='?c=rol&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                  <?php
                  }
                  ?>
                <a href='?c=rol' class='btn btn-warning'>Ver todos los registros</a>
                                
            </div>
            <hr class='d-sm-none'>
        </div></form>