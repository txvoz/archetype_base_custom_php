<?php
class PruebaController implements IController, IManagementForm, IAction  {
private $path = "rendered/prueba/";private $config = null;public function __construct() {
        $this->config = Config::singleton();
        require "{$this->config->get("entities")}Prueba.php";
        require "{$this->config->get("models")}PruebaModel.php";
        }public function index() {
        $this->viewList();
        }public function viewCreate() {
        $vars = [];
        $vars["create"] = true;
        
        View::show("{$this->path}viewForm", $vars);
        }public function viewDetail() {
        $vars = [];
        $vars["id"] = $_REQUEST["acc"];
        
        View::show("{$this->path}viewForm", $vars);
        }public function viewList() {
        View::show("{$this->path}viewList");
        }public function actionList() {
        $arg = new stdClass();
        //*******************
        $m = new PruebaModel();
        $arg->filtro = @$_REQUEST['search'];
        $arg->paginator = null;
        $rcount = $m->getCount($arg->filtro)->cantidad;
        $arg->paginator = new Paginator($rcount, @$_REQUEST['p']);
        $r = $m->get($arg, false);
        $m->lazyLoad($r->data);
        if ($arg->paginator !== null) {
            $r->paginator = $arg->paginator->renderPaginator();
        }
        $r->count = $rcount;
        echo json_encode($r);
        }public function actionListData() {
        //*******************
        $m = new PruebaModel();
        $rcount = $m->getCount()->cantidad;
        $r = $m->get();
        $r->count = $rcount;
        echo json_encode($r);
        }public function actionDetail() {
            if ($_SERVER['REQUEST_METHOD'] === "GET") {
                $r = null;
                $m = new PruebaModel();
                $e = new Prueba();
                $e->setPruId($_REQUEST["acc"]);
                $r = $m->getById($e);
                echo json_encode($r);
            } else if ($_SERVER['REQUEST_METHOD'] === "POST") {
                $this->actionUpdate();
            }
        }public function actionCreate() {
        $data = Utils::getParamsByBody();
        $e = new Prueba();
        $e->serializeByObject($data);
        $m = new PruebaModel();
        $r = $m->insert($e);
        echo json_encode($r);
        }public function actionDelete() {
        $m = new PruebaModel();
        $e = new Prueba();
        $e->setPruId($_REQUEST["acc"]);
        $r = $m->delete($e);
        echo json_encode($r);
        }public function actionUpdate() {
        $data = Utils::getParamsByBody();
        $e = new Prueba();
        $e->serializeByObject($data);
        $m = new PruebaModel();
        $r = $m->update($e);
        echo json_encode($r);
        }
}